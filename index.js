import puppeteer from "puppeteer";
import fs from "fs";

const changeRegion = async (page, newRegion) => {
  await (await page.waitForSelector('span[class*="Region_region"')).click();
  await page.waitForSelector('[class*="RegionListBase"]');
  const regionButtons = await page.$x(`//li[contains(text(),'${newRegion}')]`);
  if (regionButtons.length === 0) {
    throw new Error(errMessages.WRONG_REGION);
  }
  await Promise.all([
    page.waitForNavigation(),
    regionButtons[0].hover(),
    regionButtons[0].click(),
  ]);
};
const saveParsingData = async (result, url) => {
  const text =
    url +
    "\n" +
    "price=" +
    result.price +
    "\n" +
    "priceOld=" +
    result.priceOld +
    "\n" +
    "rating=" +
    result.rating +
    "\n" +
    "reviewCount=" +
    result.reviewCount +
    "\n";

  await fs.promises.writeFile("product.txt", text + "\n", {
    flag: "a",
  });

  const timestamp = new Date().getTime();
  const screenshotName = `${timestamp}_screenshot.jpg`;
  await fs.promises.writeFile(
    screenshotName,
    Buffer.from(result.screenshot, "base64")
  );
};

const startParse = async (browser, url, region) => {
  const result = {};

  const page = await browser.newPage();
  await page.setViewport({ width: 1080, height: 1024 });
  await page.goto(url);

  await changeRegion(page, region);

  result.screenshot = await page.screenshot({
    encoding: "base64",
    fullPage: true,
  });

  const ratingElement = await page.waitForSelector(
    "span[class*='Rating_value']"
  );
  const rating = await ratingElement.evaluate((element) => element.textContent);
  result.rating = rating;
  const reviewCountElement = await page.waitForSelector(
    "button[class*='ActionsRow_button']"
  );
  const reviewCount = await reviewCountElement.evaluate(
    (element) => element.textContent
  );
  result.reviewCount = reviewCount;

  const noDataAvailabe = await page.$(
    'div[class*="OutOfStockInformer_informer"]'
  );
  if (noDataAvailabe) {
    return result;
  }

  const priceElement = await page.waitForSelector(
    "span[class*='Price_size_XL']"
  );
  const price = await priceElement.evaluate((element) => element.textContent);
  result.price = price;
  try {
    const priceElementOld = await page.waitForSelector(
      'div[class*="ProductPage_informationBlock"] span[class*="Price_role_old"]',
      { timeout: 2500 }
    );
    const priceOld = await priceElementOld.evaluate(
      (element) => element.textContent
    );
    result.priceOld = priceOld;
  } catch (error) {
    console.log("Старой цены нет");
  }

  return result;
};

async function run() {
  const url = process.argv[2];
  const region = process.argv[3];
  if (!url || !region) {
    throw Error("Не передан адрес или регион");
  }

  const browser = await puppeteer.launch({ headless: false });
  const parseResult = await startParse(browser, url, region);
  await saveParsingData(parseResult, url);
  await browser.close();
}

await run();
